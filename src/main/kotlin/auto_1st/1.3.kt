package auto_1st

fun MutableList<Int>.sqr(): MutableList<Int> = this.map { it * it }.toMutableList()

fun main() {
    val mList = mutableListOf(1, 4, 9, 16, 25)
    println(mList.sqr())
}