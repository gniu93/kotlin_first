package auto_1st

class IncorrectData(message: String): Exception(message)
fun printPersonalInformation(name: String,
                             surname: String,
                             patronymic: String? = null,
                             gender: String,
                             age: UInt,
                             dateOfBirth: String,
                             inn: Int? = null,
                             snils: String? = null) {
    //лень заморачиваться с регулярками
    if (name.isEmpty()) throw IncorrectData("Имя не может быть пустым") else print("name = $name")
    if (surname.isEmpty()) throw IncorrectData("Фамилия не может быть пустой") else print(", surname = $surname")
    if (patronymic != null) print(", patronymic = $patronymic")
    if (gender.isEmpty()) throw IncorrectData("Пол не может быть пустым") else print(", gender = $gender")
    if (age == 0u) throw IncorrectData("Некорректный возраст") else print(", age = $age")
    if (gender.isEmpty()) throw IncorrectData("Дата рождения не может быть пустой") else print(", dateOfBirth = $dateOfBirth")
    if (inn != null) print(", inn = $inn")
    if (snils != null) print(", snils = $snils")
    println()
}

fun main() {
    printPersonalInformation(age = 5u, name = "рр", dateOfBirth = "1980-01-04", gender = "M", surname = "222", inn = null)
    printPersonalInformation(age = 5u, name = "111", dateOfBirth = "1980-01-04", gender = "M", surname = "222",
        snils = "234423-234-324", inn = 1234567890, patronymic = "333")
}