package auto_1st

class Сounter {
    companion object СounterCompanion {
        private var count: UInt = 0u
        val COUNTER get() = "Вызван counter. Количество вызовов = $count".also { count++ }
    }
}

fun main(){
    println(Сounter.COUNTER)
    println(Сounter.COUNTER)
    println(Сounter.COUNTER)
}