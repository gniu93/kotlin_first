package auto_1st

import java.time.LocalDate
import kotlin.math.round

fun typeCasting(any: Any? = null) {
    when (any) {
        is String -> println("Я получил String = $any, ее длина равна ${any.length}")
        is Int -> println("Я получил Int = $any, его квадрат равен ${any * any}")
        is Double -> println("Я получил Double = $any, это число округляется до ${round(any * 100) / 100}")
        is LocalDate -> println("Я получил LocalData = $any, эта дата${if (any >= LocalDate.of(2006,12,24)) " не" else ""}" +
                " меньше чем дата основания Tinkoff")
        null -> println("Объект = null")
        else -> println("Мне этот тип неизвестен(")
    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(LocalDate.of(1990,1,1))
    typeCasting(LocalDate.of(2006,12,23))
    typeCasting(LocalDate.of(2006,12,24))
    typeCasting(LocalDate.of(2006,12,25))
    typeCasting(UByte)
    typeCasting(null)
}