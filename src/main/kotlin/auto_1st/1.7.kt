package auto_1st

fun printVararg(vararg strings: String) {
    if (strings.size % 10 != 1) print("Передано ${strings.size} ") else print("Передан ${strings.size} ")
    when (strings.size % 10) {
        1 -> print("элемент:")
        2,3,4 -> print("элемента:")
        0,5,6,7,8,9 -> print("элементов:")
    }
    println()
    var elements: String = ""
    strings.forEach {elements += "$it;" }
    println(elements.substring(0, elements.length - 1))
}

fun main() {
    printVararg("dff", "dafdsf", "dfdsf","dff", "dafdsf", "dfdsf")
    printVararg("dff")
}