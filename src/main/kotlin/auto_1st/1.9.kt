package auto_1st

data class Person(val name: String,
                  val surname: String,
                  val patronymic: String? = null,
                  val gender: String,
                  val age: UInt,
                  val dateOfBirth: String,
                  val inn: Int? = null,
                  val snils: String? = null)

fun getPerson(name: String,
                             surname: String,
                             patronymic: String? = null,
                             gender: String,
                             age: UInt,
                             dateOfBirth: String,
                             inn: Int? = null,
                             snils: String? = null): Person {
    if (name.isEmpty()) throw IncorrectData("Имя не может быть пустым")
    if (surname.isEmpty()) throw IncorrectData("Фамилия не может быть пустой")
    if (gender.isEmpty()) throw IncorrectData("Пол не может быть пустым")
    if (age == 0u) throw IncorrectData("Некорректный возраст")
    if (gender.isEmpty()) throw IncorrectData("Дата рождения не может быть пустой")

    return Person(name, surname, patronymic, gender, age, dateOfBirth, inn, snils)
}

fun main() {
    println(getPerson(age = 5u, name = "рр", dateOfBirth = "1980-01-04", gender = "M", surname = "222", inn = null))
    println(getPerson(age = 5u, name = "111", dateOfBirth = "1980-01-04", gender = "M", surname = "222",
        snils = "234423-234-324", inn = 1234567890, patronymic = "333"))
}