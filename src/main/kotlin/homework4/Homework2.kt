package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    val percentagePerItem: Float = 100F / marks.size //вес одного ученика в процентах
    var assessment2 = 0F //процент двоечников
    var assessment3 = 0F
    var assessment4 = 0F
    var assessment5 = 0F

    marks.forEach { assessment ->
        when (assessment) {
            2 -> assessment2 += percentagePerItem
            3 -> assessment3 += percentagePerItem
            4 -> assessment4 += percentagePerItem
            5 -> assessment5 += percentagePerItem
        }
    }

    println("Всего учеников ${marks.size}")
    println("отличников: $assessment5 %")
    println("хорошистов: $assessment4 %")
    println("троечников: $assessment3 %")
    println("двоечников: $assessment2 %")
}