package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var even = 0 //четные монеты

    myArray.forEach { coin ->
        even += if (coin % 2 == 0) 1 else 0
    }

    print("Всего монет ${myArray.size}. ")
    print("Капитан Запанированный Джо получит $even четных монет. ")
    print("Команда получит ${myArray.size - even} не четных моент")
}
