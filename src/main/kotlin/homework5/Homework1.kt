package homework5

//write your classes here
abstract class Animal(open val name: String,
                       var height: Int = 1,
                       var weight: Int = 1,
                       var foodPreferences: Array<String> = arrayOf(""),
                       var satiety: Int = 1) {
    fun eat(food: String) {
        if (food in foodPreferences) satiety++
    }
}
class Lion(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("мясо"),
            satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Tiger(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("мясо"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Hippopotamus(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("трава"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Wolf(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("мясо"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Giraffe(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("трава"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Elephant(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("трава"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Chimpanzee(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("банан"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
class Gorilla(name: String, height: Int = 1, weight: Int = 1, foodPreferences: Array<String> = arrayOf("банан"),
           satiety: Int = 0): Animal(name, height, weight, foodPreferences, satiety) {
}
fun eat(animals: Array<Animal>, foods: Array<String>) {
    for (i in animals.indices)
        if (i in foods.indices) animals[i].eat(foods[i])
        else println("Для ${animals[i].name} еды нет :(")
}

fun main() {
    val animal1 = Lion("sdf1", height = 5)
    val animal2 = Lion("sdf2", foodPreferences = arrayOf("2"), weight = 10)
    val animal3 = Lion("sdf3", foodPreferences = arrayOf("3"), satiety = 8)
    val animal4 = Lion("sdf4", foodPreferences = arrayOf("4"))
    val animal5 = Lion("sdf5", foodPreferences = arrayOf("5"), satiety = 5)

    println(animal1.satiety)
    eat(arrayOf(animal1, animal2, animal3, animal4, animal5), arrayOf("мясо", "2", "3", "4"))
    println(animal1.satiety)
    animal1.eat("2")
    println(animal1.satiety)
    println("---")
    println(animal1.height)
    animal1.height = 11
    println(animal1.height)
    println(animal1.weight)
}