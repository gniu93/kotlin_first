package homework6

//write you classes here
class User(val login: String, val password: String)

class WrongLoginException(message: String): Exception(message)
class WrongPasswordException(message: String): Exception(message)

fun createUser(login: String?, password: String?, passwordConfirmation: String?): User? {
    fun validateLogin(login: String?) {
        if (login == null) throw WrongLoginException("login не может быть null.")
        if (login.isEmpty()) throw WrongLoginException("Логин не может быть пустым.")
        if (login.length > 20) throw WrongLoginException("Логин не может быть больше 20 символов.")
    }
    fun validatePassword(password: String?, passwordConfirmation: String?) {
        if (password == null) throw WrongPasswordException("password не может быть null.")
        if (passwordConfirmation == null) throw WrongPasswordException("passwordConfirmation не может быть null.")
        if (password.length < 10) throw WrongPasswordException("Пароль не может быть меньше 10 символов.")
        if (password != passwordConfirmation) throw WrongPasswordException("Пароли не совпадают.")
    }

    return try {
        validateLogin(login)
        validatePassword(password, passwordConfirmation)
        User(login ?: "NullUser", password ?: "NullPassword")
    }
    catch (e: WrongLoginException) {
        println(e.message)
        null
    }
    catch (e: WrongPasswordException) {
        println(e.message)
        null
    }
}

fun main() {
    val user2 = createUser("null", "null", null)

    println("Введите логин от 1 до 20 символов")
    val login = readln()
    println("Введите пароль не менее 10 символов")
    val password = readln()
    println("Подтвердите пароль")
    val passwordConfirmation = readln()
    val user1 = createUser(login, password, passwordConfirmation)

    if (user1 != null) println("Пользователь \"${user1.login}\" успешо создан")
}

//TODO: Homework2 write your function here