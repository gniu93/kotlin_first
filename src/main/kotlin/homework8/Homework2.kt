package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun vegetablesCountInCart(cart: Map<String, Int>, vegetableSet: Set<String>): Int {
    var vegetablesCount = 0
    cart.forEach { if (vegetableSet.contains(it.key)) vegetablesCount += it.value }
    return vegetablesCount
}

class NoPriceForProduct(message: String): Exception(message)
class MinusOrNullPrice(message: String): Exception(message)
class MinusOrNullCount(message: String): Exception(message)
class IncorrectDiscount(message: String): Exception(message)
fun cartCost(prices: Map<String, Double>, cart: Map<String, Int>, discountValue: Double, discountSet: Set<String>): Double {
    if (discountValue >= 1 || discountValue <= 0) throw IncorrectDiscount("Не корректная скидка : $discountValue")
    var cost = 0.0
    cart.forEach{
        if (!prices.contains(it.key)) throw NoPriceForProduct("Нет цены на ${it.key}")
        if (prices.getValue(it.key) <= 0) throw MinusOrNullPrice("Цена на товар не может быть <= 0. ${it.key} : ${prices.getValue(it.key)}")
        if (it.value <= 0) throw MinusOrNullCount("Количество товара не может быть <= 0. ${it.key} : ${it.value}")

        cost += if (discountSet.contains(it.key)) {
            prices.getValue(it.key) * it.value * (1 - discountValue)
        } else {
            prices.getValue(it.key) * it.value
        }
    }
    return cost
}

fun main() {
    //call your functions here
    println(vegetablesCountInCart(userCart, vegetableSet))
    println(cartCost(prices, userCart, discountValue, discountSet))
}
