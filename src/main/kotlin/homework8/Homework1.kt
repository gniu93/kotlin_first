package homework8

fun removeDuplicates(originalList: List<Int>) = originalList.toSet().toList()
fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    //call your function here
    println(removeDuplicates(sourceList))
}
