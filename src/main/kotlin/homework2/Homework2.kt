fun main() {
    val lemonade: Int = 18500 //18500 мл лимонада
    val pinaColada: Short = 200 //200 мл пина колады
    val whiskey: Byte = 50 //50 мл виски
    val fresh: Long = 3000000000L //3000000000 капель фреша
    val cola: Float = 0.5F //0.5 литра колы
    val ale: Double = 0.666666667 //0.666666667 литра эля
    val signatureDrink: String = "Что-то авторское!" //“Что-то авторское!”

    println("Заказ - ‘$lemonade мл лимонада’ готов!")
    println("Заказ - ‘$pinaColada мл пина колады’ готов!")
    println("Заказ - ‘$whiskey мл виски’ готов!")
    println("Заказ - ‘$fresh капель фреша’ готов!")
    println("Заказ - ‘$cola литра колы’ готов!")
    println("Заказ - ‘$ale  литра эля’ готов!")
    println("Заказ - ‘$signatureDrink’ готов!")

}