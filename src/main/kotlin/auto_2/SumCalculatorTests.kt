package auto_2
import junit.framework.TestCase.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test

class CalculatorTests
{
    @Test
    fun sumOfTwoPositiveNumbersWithinADigit() {
        val a = 1.1
        val b = 2.3
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfTwoPositiveNumbersWithDigitChange() {
        val a = 199.0
        val b = 950.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfTwoNegativeNumbersWithinADigit() {
        val a = -1.0
        val b = -2.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfTwoNegativeNumbersWithDigitChange() {
        val a = -199.0
        val b = -950.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfNegativeAndPositiveNumbers() {
        val a = 1000.0
        val b = -1.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfANumberWithZero() {
        val a = 3434.0
        val b = 0.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfTwoZeros() {
        val a = 0.0
        val b = 0.0
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun sumOfNumbersBeyondTypeLimits() {
        val a = Double.MAX_VALUE
        val b = Double.MAX_VALUE
        val expected = Double.POSITIVE_INFINITY
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }
    @Test
    fun amountWithNan() {
        val a = Double.NaN
        val b = Double.MAX_VALUE
        val expected = a + b
        assertEquals("Ошибка сложения $a и $b", expected, calculator(sum, a, b))
    }

    @Test
    fun subtractionOfTwoPositiveNumbersWithinADigit() {
        val a = 5.1
        val b = 2.2
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfTwoPositiveNumbersWithDigitChange() {
        val a = 199.0
        val b = 950.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfTwoNegativeNumbersWithinADigit() {
        val a = -1.0
        val b = -2.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfTwoNegativeNumbersWithDigitChange() {
        val a = -100.0
        val b = -10.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfNegativeAndPositiveNumbers() {
        val a = 3.0
        val b = -1.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfANumberWithZero() {
        val a = 3434.0
        val b = 0.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfTwoZeros() {
        val a = 0.0
        val b = 0.0
        val expected = a - b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionOfNumbersBeyondTypeLimits() {
        val a = Double.MAX_VALUE
        val b = -Double.MAX_VALUE
        val expected = Double.POSITIVE_INFINITY
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }
    @Test
    fun subtractionWithNan() {
        val a = Double.NaN
        val b = Double.MAX_VALUE
        val expected = a + b
        assertEquals("Ошибка вычитания $a и $b", expected, calculator(subtraction, a, b))
    }

    @Test
    fun dividingTwoNumbersWithoutAFractionalPart() {
        val a = 6.0
        val b = 2.0
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun dividingTwoNumbersWithAFractionalPart() {
        val a = 456.2
        val b = 42.5
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun dividingAPositiveNumberByANegativeNumber() {
        val a = 232123.0
        val b = -5.0
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun dividingANegativeNumberByAPositiveNumber() {
        val a = -5252.0
        val b = 22.9
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun dividingZeroByNumber() {
        val a = 0.0
        val b = 2.0
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun divisionByZero() {
        val a = 78.8
        val b = 0.0
        val expected = a / b
        assertThrows(UnsupportedOperationException::class.java) {calculator(division, a, b)}
    }
    @Test
    fun divisionByNan() {
        val a = 3098.3
        val b = Double.NaN
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun divisionNan() {
        val a = Double.NaN
        val b = 3.0
        val expected = a / b
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }
    @Test
    fun divisionBeyondTypeLimits() {
        val a = Double.MAX_VALUE
        val b = Double.MIN_VALUE
        val expected = Double.POSITIVE_INFINITY
        assertEquals("Ошибка деления $a и $b", expected, calculator(division, a, b))
    }

    @Test
    fun multiplyingTwoNumbersWithoutAFractionalPart() {
        val a = -2.0
        val b = 7.0
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplyingTwoNumbersWithAFractionalPart() {
        val a = 34.7
        val b = -94.9
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplyingZero() {
        val a = 0.0
        val b = 9.0
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplyByZero() {
        val a = 5445.0
        val b = 0.0
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplicationNan() {
        val a = Double.NaN
        val b = 5.1
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplicationByNan() {
        val a = 9.3
        val b = Double.NaN
        val expected = a * b
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
    @Test
    fun multiplicationBeyondTypeLimits() {
        val a = Double.MAX_VALUE
        val b = Double.MAX_VALUE
        val expected = Double.POSITIVE_INFINITY
        assertEquals("Ошибка умножения $a и $b", expected, calculator(multiply, a, b))
    }
}