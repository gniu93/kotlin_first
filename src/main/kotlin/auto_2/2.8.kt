package auto_2

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}
class IncorrectInn(message: String): Exception(message)
fun main() {
    val inn = "705733025741"

    val validateInn = { it: String ->

        if (inn.matches("[0-9]{12}".toRegex()) == false)
            throw IncorrectInn("Не корректный формат ИНН, ожидается 12 цифр без разделителей")

        val numbers = it.map { it.digitToInt() }

        val firstOdds = arrayOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
        val firstCheckDigit = numbers
            .take(10)
            .foldIndexed(0) {index, sum, item ->  sum + item * firstOdds[index]} % 11 % 10

        val secondOdds = arrayOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
        val secondCheckDigit = numbers
            .take(11)
            .foldIndexed(0) {index, sum, item ->  sum + item * secondOdds[index]} % 11 % 10

        (firstCheckDigit == numbers[10]) && (secondCheckDigit == numbers[11])
    }

    validateInnFunction(inn, validateInn)
}