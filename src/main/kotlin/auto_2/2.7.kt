package auto_2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiply = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double ->
    if (b == 0.0)
        throw UnsupportedOperationException("Division by zero")
    else
        a / b
}

fun main() {

    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 28.0, 32.0)
    calculator(getCalculationMethod("/"), 41.0, 5.0)

}

fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ): Double {
    return lambda(a,b)
}


fun getCalculationMethod(name: String) :(Double, Double)-> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiply
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}