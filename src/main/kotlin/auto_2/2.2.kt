package auto_2

import java.util.*

fun listModifator(list: List<Double?>): Double {
    val sum = list
        .asSequence()
        .filterNotNull()
        .map { if ((it % 2).toInt() == 0) it * it else it / 2 }
        .filter { it <= 25 }
        .sortedDescending()
        .take(10)
        .sum()

    return "%.2f".format(Locale.ENGLISH, sum).toDouble()
}

fun main() {
    println(listModifator(listOf(13.31, 3.98, 12.0, 2.99, 9.0)))
    println(listModifator(listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)))
}