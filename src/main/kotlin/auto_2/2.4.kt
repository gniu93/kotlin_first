package auto_2

abstract class Pet()

abstract class Cat(): Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a ${javaClass.simpleName}, and i running")
    }

    override fun swim() {
        println("I am a ${javaClass.simpleName}, and i swimming")
    }
}

class Tiger(): Cat()
class Lion(): Cat()

abstract class Fish(): Pet(), Swimmable {
    override fun swim() {
        println("I am a ${javaClass.simpleName}, and i swimming")
    }
}

class Salmon(): Fish()
class Tuna(): Fish()

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

fun <T: Runnable> useRunSkill(pet: T) {
    pet.run()
}

fun <T: Swimmable> useSwimSkill(pet: T) {
    pet.swim()
}

fun <T> useSwimAndRunSkill(pet: T) where T: Swimmable, T: Runnable {
    pet.run()
    pet.swim()
}

fun main() {
    val tiger = Tiger()
    useRunSkill(tiger)
    useSwimSkill(tiger)
    useSwimAndRunSkill(tiger)

    val lion = Lion()
    useRunSkill(lion)
    useSwimSkill(lion)
    useSwimAndRunSkill(lion)

    val salmon = Salmon()
    useSwimSkill(salmon)

    val tuna = Tuna()
    useSwimSkill(tuna)
}