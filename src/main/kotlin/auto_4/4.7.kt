package auto_4

import com.google.gson.GsonBuilder
import com.itextpdf.io.font.PdfEncodings
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Table
import java.io.File
import java.io.FileOutputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import kotlin.random.Random

data class Person(
    val name: String,
    val surname: String,
    val patronymic: String,
    val age: Int,
    val gender: String,
    val dateOfBirth: String,
    val placeOfBirth: String,
    val inn: String,
    val address: Address
)

data class Address(
    val postcode: Int,
    val country: String,
    val region: String,
    val city: String,
    val street: String,
    val house: Int,
    val apartment: Int
)

object Randomizer {
    private val cities = listOf("Екатеринбург", "Москва" , "Красноярск", "Иркутск", "Краснотурьинск", "Тюмень", "Челябинск",
        "Томск", "Нижний Тагил", "Верхотурье")
    private val genders = listOf("МУЖ", "ЖЕН")
    private val namesMen = listOf("Александр", "Сергей", "Алексей", "Илья", "Андрей", "Егор", "Никита", "Матвей", "Тимофей")
    private val namesWomen = listOf("Ирина", "Александра", "Светлана", "Дарья", "Анастасия", "Алиса", "Кристина", "Марина", "Екатерина")
    private val patronymics = listOf("Александров", "Егоров", "Андреев", "Игорев", "Александров", "Сергеев", "Владиславов", "Гринорьев")
    private val surname = listOf("Иванов", "Смирнов", "Кузнецов", "Козлов", "Попов", "Голубев")
    private val regions = listOf("Свердловская область", "Ленинградская область", "ХМАО", "Краснодарский край", "Ярославская область",
        "Республика Таторстан", "Томская область")
    private val streets = listOf("Проспект Ленина", "ул. Карла Ликбнехта", "ул. Московская", "ул. Малышева", "ул. 8 Марта",
        "ул. Крылова", "ул. Ключевская")

    private fun nameRandome(gender: String): String {
        if (gender == "МУЖ")
            return namesMen[Random.nextInt(namesMen.size)]
        else return namesWomen[Random.nextInt(namesWomen.size)]
    }

    private fun patronymicRandome(gender: String): String {
        return if (gender == "МУЖ")
            patronymics[Random.nextInt(patronymics.size)] + "ич"
        else patronymics[Random.nextInt(patronymics.size)] + "на"
    }

    private fun surnameRandome(gender: String): String {
        return if (gender == "МУЖ")
            surname[Random.nextInt(surname.size)]
        else surname[Random.nextInt(surname.size)] + "а"
    }

    private fun cityRandome(): String {
        return cities[Random.nextInt(cities.size)]
    }

    private fun genderRandome(): String {
        return genders[Random.nextInt(genders.size)]
    }

    private fun dateOfBirthRandome(): String {
        val year = Random.nextInt(1980, 2023)
        val month = Random.nextInt(1, 12)
        val day = Random.nextInt(1, 28)
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        return LocalDate.of(year,month,day).format(formatter).toString()
    }

    private fun addressRandome(): Address {
        val address = Address(
            postcode = Random.nextInt(100000, 999999),
            country = "Россия",
            region = regions[Random.nextInt(regions.size)],
            city = cities[Random.nextInt(cities.size)],
            street = streets[Random.nextInt(streets.size)],
            house = Random.nextInt(1, 150),
            apartment = Random.nextInt(1, 300)
        )
        return address
    }

    private fun getCheckDigit(numbers: MutableList<Int>, odds: Array<Int>): Int {
        return numbers.zip(odds)
            .sumOf { it.first * it.second } % 11 % 10
    }
    private fun innRandome(): String {
        val taxAuthorityCode = "7075".map { it.digitToInt() } //"7075"
        val serialNumberOfTheRecord = (1..6).map { Random.nextInt(0, 9) }

        val numbers = mutableListOf<Int>()
        numbers.addAll(taxAuthorityCode)
        numbers.addAll(serialNumberOfTheRecord)

        val firstOdds = arrayOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
        val firstCheckDigit = getCheckDigit(numbers, firstOdds)

        numbers.add(firstCheckDigit)
        val secondOdds = arrayOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
        val secondCheckDigit = getCheckDigit(numbers, secondOdds)

        numbers.add(secondCheckDigit)

        return numbers.joinToString("")
    }

    private fun randomPerson(): Person {
        val gender = genderRandome()
        val dateOfBirth = dateOfBirthRandome()
        return Person(name = nameRandome(gender),
            surname = surnameRandome(gender),
            patronymic = patronymicRandome(gender),
            age = ChronoUnit.YEARS.between(LocalDate.parse(dateOfBirth, DateTimeFormatter.ofPattern("dd-MM-yyyy")),
                LocalDate.now()).toInt(),
            gender = gender,
            dateOfBirth = dateOfBirth,
            placeOfBirth = cityRandome(),
            inn = innRandome(),
            address = addressRandome())
    }

    fun randomePersons(n: Int): MutableList<Person> {
        val persons: MutableList<Person> = mutableListOf()
        for (i in 0 until  n) {
            persons.add(randomPerson())
        }
        return persons
    }
}

object Exports {
    fun exportToJson(persons: MutableList<Person>) {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val jsonString = gson.toJson(persons)
        val filePath = System.getProperty("user.dir") + "/persons.json"
        File(filePath).writeText(jsonString, Charsets.UTF_8)
        println("Файл создан. Путь: $filePath")
    }
    fun exportToPdf(persons: MutableList<Person>) {
        val filePath = System.getProperty("user.dir") + "/persons.pdf"
        createPDF(persons, filePath)
        println("Файл создан. Путь: $filePath")
    }

    private fun createPDF(persons: MutableList<Person>, filePath: String) {
        val headersTitel = listOf("Имя", "Фамилия", "Отчество", "Возраст", "Пол", "Дата рождения", "Место рождения", "ИНН",
            "Индекс", "Страна", "Область", "Город", "Улица", "Дом", "Квартира")
        val writer = PdfWriter(FileOutputStream(filePath))
        val pdf = PdfDocument(writer)
        val document = Document(pdf)
        pdf.defaultPageSize = PageSize.A3.rotate()
        val table = Table(headersTitel.size)
        val font = PdfFontFactory.createFont("arialuni.ttf", PdfEncodings.IDENTITY_H, PdfFontFactory.EmbeddingStrategy.FORCE_EMBEDDED)
        table.setFont(font)
        headersTitel.forEach{table.addHeaderCell(it)}

        persons.forEach {
            table.addCell(it.name)
            table.addCell(it.surname)
            table.addCell(it.patronymic)
            table.addCell(it.age.toString())
            table.addCell(it.gender)
            table.addCell(it.dateOfBirth)
            table.addCell(it.placeOfBirth)
            table.addCell(it.inn)
            table.addCell(it.address.postcode.toString())
            table.addCell(it.address.country)
            table.addCell(it.address.region)
            table.addCell(it.address.city)
            table.addCell(it.address.street)
            table.addCell(it.address.house.toString())
            table.addCell(it.address.apartment.toString())
        }
        document.add(table)

        document.close()
    }
}

fun main() {
    print("Сколько людей нужно? Введите число от 0 до 30: ")
    val personCountString = readln()
    var personCount = 0
    try {
        personCount = personCountString.toInt()
    }
    catch (e: NumberFormatException) {
        println("Некорректное число")
    }
    if (personCount !in 0 until 31) println("Ожидается число от 0 до 30")
    val persons = Randomizer.randomePersons(personCount)

    Exports.exportToJson(persons)
    Exports.exportToPdf(persons)
}
