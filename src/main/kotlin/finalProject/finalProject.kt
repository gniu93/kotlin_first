package finalProject
import kotlin.system.exitProcess

val colors = mapOf("Red" to "\u001B[31m", "Grey" to "\u001B[90m", "Green" to "\u001B[32m", "Yellow" to "\u001B[93m",
    "Cyan" to "\u001B[96m", "Bright Yellow" to "\u001B[93m")
val cellStateUser = mapOf("Empty" to "•", "Ship" to "o", "DeadShip" to "x", "Shot" to "•", "Border" to "•")
val cellStateOpponent = mapOf("Empty" to "?", "Ship" to "o", "DeadShip" to "x", "Shot" to "•", "Border" to "•")
const val tab = '\t'
val xCoordinates = arrayOf("А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "К")
val yCoordinates = arrayOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
fun getColor(color: String): String = colors.getValue(color)

class CrossingWithAnotherShip(message: String): Exception(message)
class GoingOutOfBounds(message: String): Exception(message)
class IncorrectCoordinate(message: String): Exception(message)
class MenuInput(message: String): Exception(message)
class MapSize(val size: Int, val singleDeck: Byte, val doubleDecker: Byte, val threeDeck: Byte, val fourDeck: Byte)
class User(var name: String) {
    private val ships: MutableList<Ship> = mutableListOf()
    val steps: MutableList<Point> = mutableListOf()
    fun addShip(x: Int, y: Int, vector: Int, size: Int, mapSize: Int){
        val otherShips: MutableList<Int> = mutableListOf()
        ships.forEach{it.points.forEach { otherShips.add(1000 + it.x*10 + it.y) }}
        ships.add(Ship(size))
        try {
            ships[ships.lastIndex].create(x, y, vector, otherShips, mapSize)
        }
        catch (e: CrossingWithAnotherShip) {
            ships.removeAt(ships.lastIndex)
            throw CrossingWithAnotherShip("Корабль пересекает зону другого корабля")
        }
        catch (e: GoingOutOfBounds) {
            ships.removeAt(ships.lastIndex)
            throw GoingOutOfBounds("Корабль выходит за границы поля")
        }
    }
    fun clearShips() { for (i in 0 until getShipsCount()) ships.removeAt(0)}
    fun getShipsCount() = ships.size
    fun getShipPoints(index: Int) = ships[index].points
    fun getShipBorders(index: Int) = ships[index].borderPoints
    fun getShips() = ships
}
class Point(var x: Int, var y: Int, var status: Byte = -1)
class Ship(shipSize: Int) {
    private var created = false
    var alive = true
    val points = Array(shipSize) { Point(-1,-1) } //0 - цел, 1 - gjl,bn
    val borderPoints: MutableList<Point> = mutableListOf()
    fun create(x: Int, y: Int, vector: Int, otherShips: MutableList<Int>, mapSize: Int) { //1 - вверх, 2 вниз, 3 - вправо, 4 - влево
        points[0].x = x
        points[0].y = y
        points[0].status=0
        for (i in 1 until points.size) {
            points[i].status = 0
            when (vector) {
                1 -> {
                    points[i].y = points[i - 1].y - 1
                    points[i].x = points[i-1].x
                }
                2 -> {
                    points[i].y = points[i - 1].y + 1
                    points[i].x = points[i-1].x
                }
                3 -> {
                    points[i].x = points[i - 1].x + 1
                    points[i].y = points[i-1].y
                }
                4 -> {
                    points[i].x = points[i - 1].x - 1
                    points[i].y = points[i-1].y
                }
            }
        }

        fun minPoint(): Point {
            val point = Point(points[0].x, points[0].y,0)
            if (points.size>1)
                points.forEach {
                    if ( it.x + it.y < point.x + point.y ) {
                        point.x = it.x
                        point.y = it.y
                    }
                }
            return point
        }
        fun maxPoint(): Point {
            val point = Point(points[0].x, points[0].y,0)
            if (points.size>1)
                points.forEach {
                    if ( it.x + it.y > point.x + point.y ) {
                        point.x = it.x
                        point.y = it.y
                    }
                }
            return point
        }
        val borderX1 = minPoint().x-1
        val borderY1 = minPoint().y-1
        val borderX2 = maxPoint().x+1
        val borderY2 = maxPoint().y+1

        if ( (minPoint().x < 0) || (minPoint().y<0) || (maxPoint().x > mapSize-1) || (maxPoint().y> mapSize-1) )
            throw GoingOutOfBounds("Корабль выходит за границы поля")


        val borderPointsInt: MutableList<Int> = mutableListOf()
        for (borderX in borderX1..borderX2)
            for (borderY in borderY1..borderY2)
                if (borderX>=0 && borderY >=0)
                borderPointsInt.add(1000 + borderX*10 + borderY)
        if (borderPointsInt.intersect(otherShips.toSet()).isNotEmpty()) throw CrossingWithAnotherShip("Корабль пересекает зону другого корабля")
        points.forEach { borderPointsInt.remove(1000 + it.x*10 + it.y) }
        borderPointsInt.forEach {
            borderPoints.add(Point((it-1000) / 10, (it-1000) % 10))
        }

        created = true
    }
}

class GameDisplay(private val size: Int) {
    private val currentUserBoard = Array(size) { Array((size) ) { "" } }
    private val opponentBoard  = Array(size) { Array((size) ) { "" } }
    private val display  = Array(size+1) { Array((size*2+3) ) { "" } }
    private fun set(x: Int, y: Int, text: String, currentUser: Boolean) {
        if (currentUser) currentUserBoard[y][x] = text
        else opponentBoard[y][x] = text
    }
    fun setUser(x: Int, y: Int, text: String) {
        set(x, y, text, true)
    }
    fun setOpponent(x: Int, y: Int, text: String) {
        set(x, y, text, false)
    }
    private fun print() {
        delimiter()
        display.forEach {
            it.forEach { print(it) }
            println()
        }
    }
    fun printDisplay() {
        val offsetUserX = 1
        val offsetY = 1
        val offsetOpponentX = size + 3

        display[0][0] = tab.toString()
        for (x in 1..size) display[0][x] = getColor("Yellow")+ xCoordinates[x-1]+ tab
        for (y in 0 until size) display[y+1][0] = getColor("Yellow")+ yCoordinates[y]+ tab

        for (x in 1..size)
            for (y in 1..size)
                display[x][y]="${getColor("Grey")}${cellStateUser.getValue("Empty")}${tab}"

        for (y in 0..size)
            display[y][size+1] += "${getColor("Grey")}|${tab}"

        display[0][size+2] = tab.toString()
        for (x in 0 until size) display[0][size+3+x] = getColor("Yellow")+ xCoordinates[x]+ tab
        for (y in 0 until size) display[y+1][size+2] = getColor("Yellow")+ yCoordinates[y]+ tab

        for (x in 1..size)
            for (y in 1..size)
                display[x][y + size+2]="${getColor("Grey")}${cellStateOpponent.getValue("Empty")}${tab}"

        for (x in 0 until size)
            for (y in 0 until size) {
                if (currentUserBoard[y][x] != "") display[y+offsetY][x+offsetUserX] = currentUserBoard[y][x]
                if (opponentBoard[y][x] != "") display[y+offsetY][x+offsetOpponentX] = opponentBoard[y][x]
            }

        print()
    }
}
fun delimiter() = println("\u001B[93m----------")
object Game {
    val mapSizes = mutableMapOf("1: Маленькая" to MapSize(4, 2, 1, 0, 0),
        "2: Средняя" to MapSize(7, 3, 2, 1, 0),
        "3: Большая" to MapSize(10, 4, 3, 2, 1))
    var mapSize = "1: Маленькая"
    val user1 = User("")
    val user2 = User("")
    private var currentPlayerUser1 = true
    private fun printPlayingDesk(showBorders: Boolean = false) {
        if (currentPlayerUser1) println("Игрок: ${user1.name}") else println("Игрок: ${user2.name}")
        val size = mapSizes.getValue(mapSize).size
        val gameDisplay = GameDisplay (size)
        //отмечаем выстрелы в нас
        val stepsOpponent = if (currentPlayerUser1) user2.steps else user1.steps
        stepsOpponent.forEach{gameDisplay.setUser(it.x, it.y, "${getColor("Red")}${cellStateUser.getValue("Shot")}$tab")}
        //
        //отмечаем выстрелы в противника
        val stepsSelf = if (currentPlayerUser1) user1.steps else user2.steps
        stepsSelf.forEach{gameDisplay.setOpponent(it.x, it.y, "${getColor("Red")}${cellStateOpponent.getValue("Shot")}$tab")}
        //

        //размещаем корабли
        val shipsCount = if (currentPlayerUser1) user1.getShipsCount() else user2.getShipsCount()
        if (shipsCount>0)
            for (n in 0 until shipsCount) {
                val shipPoints = if (currentPlayerUser1) user1.getShipPoints(n) else user2.getShipPoints(n)
                shipPoints.forEach {
                    if (it.status == (0).toByte())
                        gameDisplay.setUser(it.x,it.y, "${getColor("Green")}${cellStateUser.getValue("Ship")}$tab")
                    else gameDisplay.setUser(it.x, it.y, "${getColor("Red")}${cellStateUser.getValue("DeadShip")}$tab")

                }

                if (showBorders) {
                    val shipBorders = if (currentPlayerUser1) user1.getShipBorders(n) else user2.getShipBorders(n)
                    shipBorders.forEach {
                        if ((it.x>=0 && it.y>=0) && (it.x<=size-1 && it.y<=size-1))
                            gameDisplay.setUser(it.x, it.y,"${getColor("Cyan")}${cellStateUser.getValue("Border")}$tab")
                    }
                }
            }
        //закончили размещать корабли

        //размещаем корабли противника
        val opponentShips = if (currentPlayerUser1) user2.getShips() else user1.getShips()
        opponentShips.forEach{
            it.points.forEach {
                if (it.status == (1).toByte() )
                    gameDisplay.setOpponent(it.x,it.y, "${getColor("Red")}${cellStateOpponent.getValue("DeadShip")}$tab")
            }
            if (!it.alive) {
                it.borderPoints.forEach{
                    if ((it.x>=0 && it.y>=0) && (it.x<=size-1 && it.y<=size-1))
                        gameDisplay.setOpponent(it.x,it.y, "${getColor("Red")}${cellStateOpponent.getValue("Border")}$tab")
                }
            }
        }
        //
        gameDisplay.printDisplay()
    }
    fun start() {
        toMenu()
    }
    private fun getX(): Int {
        var x = -1
        while (x == -1) {
            try {
                print("\u001B[32mКоордината X: ")
                x = xCoordinates.indexOf(readln().uppercase())
                if (x == -1) throw IncorrectCoordinate("Некорректная координата")
                if (x > mapSizes.getValue(mapSize).size - 1) {
                    x = -1
                    throw IncorrectCoordinate("Некорректная координата")
                }
            } catch (e: IncorrectCoordinate) {
                println("${getColor("Red")}${e.message}")
            }
        }
        return x
    }
    private fun getY(): Int {
        var y = -1
        while (y == -1) {
            try {
                print("\u001B[32mКоордината Y: ")
                y = readln().toInt() - 1
                if ((y > mapSizes.getValue(mapSize).size - 1) || (y < 0)) {
                    y = -1
                    throw IncorrectCoordinate("Некорректная координата")
                }
            } catch (e: NumberFormatException) {
                println("${getColor("Red")}Некорректная координата")
            } catch (e: IncorrectCoordinate) {
                println("${getColor("Red")}${e.message}")
            }
        }
        return y
    }
    private fun placeShips(user: User, random: Boolean = false) {
        fun place(size: Int, random: Boolean = false) {
            fun getVector(): Int {
                print("\u001B[32mВектор направления (1 - вверх, 2 вниз, 3 - вправо, 4 - влево): ")
                return readln().toInt()
            }

            val count = when (size) {
                1 -> mapSizes.getValue(mapSize).singleDeck
                2 -> mapSizes.getValue(mapSize).doubleDecker
                3 -> mapSizes.getValue(mapSize).threeDeck
                4 -> mapSizes.getValue(mapSize).fourDeck
                else -> 0
            }
            if (count>0)
                for (i in 0 until count) {
                    var success = true
                    while (success)
                        try {
                            if (!random) printPlayingDesk(true)
                            if (!random)  when (size) {
                                1 -> println("${getColor("Yellow")}Разместите однопалубный корабль")
                                2 -> println("${getColor("Yellow")}Разместите двухпалубный корабль")
                                3 -> println("${getColor("Yellow")}Разместите трехпалубный корабль")
                                4 -> println("${getColor("Yellow")}Разместите четырехполубный корабль")
                            }
                            val x = if (!random) getX() else (0 until mapSizes.getValue(mapSize).size).random()
                            val y = if (!random) getY() else (0 until mapSizes.getValue(mapSize).size).random()
                            val vector = if (size>1) {if (!random) getVector() else (1..4).random()} else 0
                            user.addShip(x,y,vector, size, mapSizes.getValue(mapSize).size)
                            success = false
                        } catch (e: CrossingWithAnotherShip) {
                            if (!random) println("${getColor("Red")}${e.message}")
                        } catch (e: GoingOutOfBounds) {
                            if (!random) println("${getColor("Red")}${e.message}")
                        }
                }
        }

        for (i in 1..4) place(i, random)
    }
    private fun switchUser() {
        currentPlayerUser1 = !currentPlayerUser1
    }
    private fun step(): Boolean {
        fun fire(): Boolean {
            var pointlessMove = true
            var x = -1
            var y = -1
            while (pointlessMove) {
                pointlessMove = false
                println("${getColor("Yellow")}Куда стреляем?")
                x = getX()
                y = getY()
                val lastSteps = if (currentPlayerUser1) user1.steps else user2.steps
                lastSteps.forEach { if ((it.x == x) && (it.y == y)) pointlessMove = true}
                if (pointlessMove) println("${getColor("Red")}Бессмысленный ход")
            }
            if (currentPlayerUser1) user1.steps.add(Point(x,y)) else user2.steps.add(Point(x,y))

            var hit = false
            val ships = if (currentPlayerUser1) user2.getShips() else user1.getShips()
            ships.forEach{
                it.points.forEach {
                    if (it.x == x && it.y == y) {
                        it.status = 1
                        hit = true
                    }
                }
                var alive = false
                it.points.forEach { if (it.status == 0.toByte()) alive = true }
                if (!alive) it.alive = false
            }
            printPlayingDesk()

            return hit
        }
        fun victory(): Boolean {
            val shipCount = if (currentPlayerUser1) user2.getShipsCount() else user1.getShipsCount()
            var victory = true
            for (i in 0 until  shipCount) {
                val ship = if (currentPlayerUser1) user2.getShipPoints(i) else user2.getShipPoints(i)
                ship.forEach { if (it.status == (0).toByte()) victory = false}
            }
            return victory
        }
        delimiter()
        printPlayingDesk()
        var hit = fire()
        var victory = victory()
        while (hit && !victory) {
            println("${getColor("Green")}Попали!")
            hit = fire()
            victory = victory()
        }
        return victory
    }
    private fun runGame() {
        println("Игра началась!")
        if (user1.name[0]=='r') placeShips(user1, true) else placeShips(user1)
        switchUser()
        if (user2.name[0]=='r') placeShips(user2, true) else placeShips(user2)
        switchUser()
        var victory = false
        while (!victory) {
            victory = step()
            if (!victory) switchUser()
        }
        println(delimiter())
        if (currentPlayerUser1) println("Игрок ${user1.name} победил!") else println("Игрок ${user2.name} победил!")
        user1.clearShips()
        user2.clearShips()
        toMenu()
    }
    private fun toMenu() {
        when (GameInterface.mainMenu()) {
            "RunGame" -> runGame()
        }
    }

    object GameInterface {
        private fun printMenu(menu: Array<String>) {
            delimiter()
            menu.forEach {
                println(it)
            }
            print("\u001B[32m?: ")
        }
        private fun startGame(): String {
            delimiter()
            var name = ""
            while (name == "") {
                print("Имя игрока 1: ")
                name = readln()
            }
            user1.name = name
            name = ""

            while (name == "") {
                print("Имя игрока 2: ")
                name = readln()
            }
            user2.name = name

            return "RunGame"
        }
        private fun menuInput(menu: Array<String>): Int {
            var input = -1
            while (input == -1) {
                try {
                    printMenu(menu)
                    input = readln().toInt()
                    if ((input > menu.size) or (input < 1)) {
                        input = -1
                        throw MenuInput("Не корректный ввод")
                    }
                } catch (e: NumberFormatException) {
                    println("${getColor("Red")}Не корректный ввод")
                } catch (e: MenuInput) {
                    println("${getColor("Red")}${e.message}")
                }
            }
            return input
        }
        fun mainMenu(): String {
            val input = menuInput(arrayOf("1: Настройки", "2: Начать игру", "3: Выход"))
            return when (input) {
                1 -> settingsMenu()
                2 -> startGame()
                3 -> exitProcess(0)
                else -> ""
            }
        }

        private fun settingsMenu(): String {
            val input = menuInput(arrayOf("1: Размер поля", "2: Назад"))
            return when (input) {
                1 -> mapSize()
                2 -> mainMenu()
                else -> ""
            }
        }

        private fun mapSize(): String {
            val menu: Array<String> = mapSizes.entries.map {
                it.key +
                        " (Размер ${it.value.size} x ${it.value.size}. Корабли: " +
                        "однопалубных - ${it.value.singleDeck}, " +
                        "двухпалубных - ${it.value.doubleDecker}, " +
                        "трехпалубных - ${it.value.threeDeck}, " +
                        "четырехпалубных - ${it.value.fourDeck})"
            }.toTypedArray()
            printMenu(menu)
            val input = readln().toInt()
            return when (input) {
                1 -> {mapSize = "1: Маленькая"; mainMenu()}
                2 -> {mapSize = "2: Средняя"; mainMenu()}
                3 -> {mapSize = "3: Большая"; mainMenu()}
                else -> ""
            }
        }
    }
}



fun main() {
    Game.start()
}